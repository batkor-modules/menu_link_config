<?php

namespace Drupal\menu_link_config\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Provides a delete form for config menu links.
 *
 * @internal
 */
class MenuLinkConfigDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    $entity_id = $route_match->getParameter('menu_link_plugin');

    if (is_null($entity_id)) {
      return parent::getEntityFromRouteMatch($route_match, $entity_type_id);
    }
    $entity_id = str_replace('menu_link_config:', '', $entity_id);
    $entity = $this
      ->entityTypeManager
      ->getStorage('menu_link_config')
      ->load($entity_id);

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    if ($this->moduleHandler->moduleExists('menu_ui')) {
      return new Url('entity.menu.edit_form', ['menu' => $this->entity->getMenuName()]);
    }

    return $this->entity->toUrl();
  }

}

<?php

/**
 * @file
 * Contains \Drupal\menu_link_config\Plugin\Menu\Form\MenuLinkConfigForm.
 */

namespace Drupal\menu_link_config\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\Form\MenuLinkFormInterface;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Menu\MenuParentFormSelectorInterface;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Routing\MatchingRouteNotFoundException;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MenuLinkConfigForm extends EntityForm {

  /**
   * The edited menu link.
   *
   * @var \Drupal\menu_link_config\Entity\MenuLinkConfig
   */
  protected $entity;

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The menu parent form selector.
   *
   * @var \Drupal\Core\Menu\MenuParentFormSelectorInterface
   */
  protected $menuParentSelector;

  /**
   * Constructs a new MenuLinkConfigForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager
   *   The menu link manager.
   * @param \Drupal\Core\Menu\MenuParentFormSelectorInterface $menu_parent_form_selector
   *   The menu parent form selector.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\Core\Path\AliasManagerInterface $alias_manager
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   */
  public function __construct(EntityTypeManager $entity_type_manager, MenuLinkManagerInterface $menu_link_manager, MenuParentFormSelectorInterface $menu_parent_form_selector, AccessManagerInterface $access_manager, AccountInterface $account, AliasManagerInterface $alias_manager, ModuleHandlerInterface $module_handler, TranslationInterface $string_translation) {
    $this->entityTypeManager = $entity_type_manager;
    $this->menuLinkManager = $menu_link_manager;
    $this->menuParentSelector = $menu_parent_form_selector;
    $this->accessManager = $access_manager;
    $this->account = $account;
    $this->pathAliasManager = $alias_manager;
    $this->setModuleHandler($module_handler);
    $this->setStringTranslation($string_translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.menu.link'),
      $container->get('menu.parent_form_selector'),
      $container->get('access_manager'),
      $container->get('current_user'),
      $container->get('path.alias_manager'),
      $container->get('module_handler'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    parent::form($form, $form_state);

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Menu link title'),
      '#default_value' => $this->entity->getTitle(),
      '#weight' => -10,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => 128,
      '#machine_name' => [
        'source' => ['title'],
        'exists' => [$this, 'exists'],
      ],
      '#disabled' => !$this->entity->isNew(),
      '#default_value' => $this->entity->id(),
      '#weight' => -9,
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Shown when hovering over the menu link.'),
      '#default_value' => $this->entity->getDescription(),
      '#weight' => -5,
    ];
    $default_url = '';
    if ($url = $this->entity->getUrlObject()) {
      $default_url = $url->toString();
      $link = [
        '#type' => 'link',
        '#title' => $this->entity->getTitle(),
      ] + $url->toRenderArray();
      $form['info'] = [
        'link' => $link,
        '#type' => 'item',
        '#title' => $this->t('Link'),
      ];
    }

    $form['url'] = [
      '#title' => $this->t('Link path'),
      '#type' => 'textfield',
      '#description' => $this->t('The path for this menu link. This can be an internal Drupal path such as %add-node or an external URL such as %drupal. Enter %front to link to the front page.', [
        '%front' => '<front>',
        '%add-node' => '/node/add',
        '%drupal' => 'http://drupal.org',
      ]),
      '#default_value' => $default_url,
      '#required' => TRUE,
      '#weight' => -2,
      '#maxlength' => 2048,
      '#element_validate' => [[LinkWidget::class, 'validateUriElement']],
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable menu link'),
      '#description' => $this->t('Menu links that are not enabled will not be listed in any menu.'),
      '#default_value' => $this->entity->status(),
    ];

    $form['expanded'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show as expanded'),
      '#description' => $this->t('If selected and this menu link has children, the menu will always appear expanded.'),
      '#default_value' => $this->entity->isExpanded(),
    ];

    $menu_parent = $this->entity->getMenuName() . ':' . $this->entity->getParent();
    $form['menu_parent'] = $this->menuParentSelector->parentSelectElement($menu_parent, $this->entity->getPluginId());
    $form['menu_parent']['#title'] = $this->t('Parent link');
    $form['menu_parent']['#description'] = $this->t('The maximum depth for a link and all its children is fixed. Some menu links may not be available as parents if selecting them would exceed this limit.');
    $form['menu_parent']['#attributes']['class'][] = 'menu-title-select';

    $delta = max(abs($this->entity->getWeight()), 50);
    $form['weight'] = [
      '#type' => 'number',
      '#min' => -$delta,
      '#max' => $delta,
      '#default_value' => $this->entity->getWeight(),
      '#title' => $this->t('Weight'),
      '#description' => $this->t('Link weight among links in the same menu at the same depth. In the menu, the links with high weight will sink and links with a low weight will be positioned nearer the top.'),
    ];

    return $form;
  }

  /**
   * Determines if the action already exists.
   *
   * @param string $id
   *   The menu item config ID.
   *
   * @return bool
   *   TRUE if the menu item exists, FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists($id) {
    $action = $this
      ->entityTypeManager
      ->getStorage('menu_link_config')
      ->load($id);
    return !empty($action);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // The entity is rebuilt in parent::submit().
    $menu_link = $this->entity;
    $saved = $menu_link->save();

    if ($saved) {
      $this->messenger()->addMessage($this->t('The menu link has been saved.'));
      $form_state->setRedirect(
        'entity.menu.edit_form',
        ['menu' => $menu_link->getMenuName()]
      );
    }
    else {
      $this->messenger()->addMessage($this->t('There was an error saving the menu link.'), 'error');
      $form_state['rebuild'] = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    $entity_id = $route_match->getParameter('menu_link_plugin');

    if (is_null($entity_id)) {
      return parent::getEntityFromRouteMatch($route_match, $entity_type_id);
    }
    $entity_id = str_replace('menu_link_config:', '', $entity_id);
    $entity = $this
      ->entityTypeManager
      ->getStorage('menu_link_config')
      ->load($entity_id);

    return $entity;
  }

}

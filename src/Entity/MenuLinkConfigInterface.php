<?php

namespace Drupal\menu_link_config\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides interface for Menu link config entity.
 */
interface MenuLinkConfigInterface extends ConfigEntityInterface {

  /**
   * Return menu link plugin id..
   *
   * @return string
   *   The menu link config plugin id.
   */
  public function getPluginId();

  /**
   * Gets the title of the menu link.
   *
   * @return string
   *   The title of the link.
   */
  public function getTitle();

  /**
   * Gets the description of the menu link for the UI.
   *
   * @return string
   *   The description to use on admin pages or as a title attribute.
   */
  public function getDescription();

  /**
   * Builds up the menu link plugin definition for this entity.
   *
   * @return array
   *   The plugin definition corresponding to this entity.
   *
   * @see \Drupal\Core\Menu\MenuLinkTree::$defaults
   */
  public function getPluginDefinition(): array;

  /**
   * Returns menu link config status.
   *
   * @return bool
   *   TRUE if is enabled, otherwise FALSE.
   */
  public function isEnabled();

  /**
   * Returns whether the menu link is marked as always expanded.
   *
   * @return bool
   *   TRUE for expanded, FALSE otherwise.
   */
  public function isExpanded();

  /**
   * Returns menu link route name if supported.
   *
   * @return string
   *   This route name.
   */
  public function getRouteName();

  /**
   * Returns menu link route parameters if supported.
   *
   * @return string
   *   This route parameters.
   */
  public function getRouteParameters();

  /**
   * Returns menu link route options if supported.
   *
   * @return string
   *   This route options.
   */
  public function getOptions();

  /**
   * Gets the url object pointing to the URL of the menu link content entity.
   *
   * @return \Drupal\Core\Url
   *   A Url object instance.
   */
  public function getUrlObject();

  /**
   * Gets the menu name of the custom menu link.
   *
   * @return string
   *   The menu ID.
   */
  public function getMenuName();

  /**
   * Returns parent menu link.
   *
   * @return string
   *   The menu link parent.
   */
  public function getParent();

  /**
   * Returns the weight of the menu link content entity.
   *
   * @return int
   *   A weight for use when ordering links.
   */
  public function getWeight();
}


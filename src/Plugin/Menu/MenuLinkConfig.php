<?php

namespace Drupal\menu_link_config\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a menu link plugin based upon storage in config.
 */
class MenuLinkConfig extends MenuLinkBase implements ContainerFactoryPluginInterface {

  /**
   * Entities IDs to load.
   *
   * It is an array of entity IDs keyed by entity IDs.
   *
   * @var array
   */
  protected static $entityIdsToLoad = [];

  /**
   * {@inheritdoc}
   */
  protected $overrideAllowed = [
    'menu_name' => 1,
    'parent' => 1,
    'weight' => 1,
    'expanded' => 1,
    'enabled' => 1,
    'title' => 1,
    'description' => 1,
    'route_name' => 1,
    'route_parameters' => 1,
    'url' => 1,
    'options' => 1,
  ];

  /**
   * The config menu link entity connected to this plugin instance.
   *
   * @var \Drupal\menu_link_config\Entity\MenuLinkConfigInterface
   */
  protected $entity;

  /**
   * The config translation mapper manager.
   *
   * Used to provide the translation route in case Config Translation module is
   * installed.
   *
   * @var \Drupal\config_translation\ConfigMapperManagerInterface
   */
  protected $mapperManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityRepository = $container->get('entity.repository');
    $instance->languageManager = $container->get('language_manager');
    $instance->mapperManager = $container->get('plugin.manager.config_translation.mapper', ContainerInterface::NULL_ON_INVALID_REFERENCE);

    return $instance;
  }

  /**
   * Loads the entity associated with this menu link.
   *
   * @return \Drupal\menu_link_config\Entity\MenuLinkConfigInterface
   *   The menu link content entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the entity ID and UUID are both invalid or missing.
   */
  public function getEntity() {
    if ($this->entity) {
      return $this->entity;
    }
    $entity = NULL;
    $storage = $this->entityTypeManager->getStorage('menu_link_config');
    if (!empty($this->pluginDefinition['metadata']['entity_id'])) {
      $entity_id = $this->pluginDefinition['metadata']['entity_id'];
      // Make sure the current ID is in the list, since each plugin empties
      // the list after calling loadMultple(). Note that the list may include
      // multiple IDs added earlier in each plugin's constructor.
      static::$entityIdsToLoad[$entity_id] = $entity_id;
      $entities = $storage->loadMultiple(array_values(static::$entityIdsToLoad));
      $entity = isset($entities[$entity_id]) ? $entities[$entity_id] : NULL;
      static::$entityIdsToLoad = [];
    }
    if (!$entity) {
      // Fallback to the loading by the ID.
      $entity = $storage->load($this->getDerivativeId());
    }
    if ($entity) {
      // Clone the entity object to avoid tampering with the static cache.
      $this->entity = clone $entity;
      $this->entity = $this->entityRepository->getTranslationFromContext($this->entity);
    }

    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEditRoute() {
    return $this->getEntity()->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function isDeletable() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeleteRoute() {
    return $this->getEntity()->toUrl('delete-form');
  }

  /**
   * {@inheritdoc}
   */
  public function deleteLink() {
    $this->getEntity()->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslateRoute() {
    // @todo There should be some way for Config Translation module to alter
    //   this information in on its own.
    if ($this->mapperManager) {
      $entity_type = 'menu_link_config';
      /** @var \Drupal\menu_link_config\MenuLinkConfigMapper $mapper */
      $mapper = $this->mapperManager->createInstance($entity_type);
      $mapper->setEntity($this->getEntity());

      return [
        'route_name' => $mapper->getOverviewRouteName(),
        'route_parameters' => $mapper->getOverviewRouteParameters(),
      ];
    }
  }

  /**
   * {@inheritdoc}
   *
   * @todo Simply storing the entity type ID in a variable would alleviate the
   *   need to override this entire method.
   */
  public function updateLink(array $new_definition_values, $persist) {
    // Filter the list of updates to only those that are allowed.
    $overrides = array_intersect_key($new_definition_values, $this->overrideAllowed);
    // Update the definition.
    $this->pluginDefinition = $overrides + $this->getPluginDefinition();

    if ($persist) {
      $entity = $this->getEntity();
      foreach ($overrides as $key => $value) {
        $entity->{$key} = $value;
      }
      $this->entityTypeManager->getStorage('menu_link_config')->save($entity);
    }

    return $this->pluginDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function isTranslatable() {
    // @todo Injecting the module handler for a proper moduleExists() check
    //   might be a bit cleaner.
    return (bool) $this->mapperManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlObject($title_attribute = TRUE) {
    return $this->getEntity() ? $this->getEntity()->getUrlObject() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->pluginDefinition['title'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

}

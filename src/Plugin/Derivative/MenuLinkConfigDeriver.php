<?php

namespace Drupal\menu_link_config\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides menu links.
 */
class MenuLinkConfigDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * This entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * MenuLinkConfig constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Get all menu link config entities.
    $entities = $this
      ->entityTypeManager
      ->getStorage('menu_link_config')
      ->loadMultiple();
    /** @var \Drupal\menu_link_config\Entity\MenuLinkConfigInterface $menu_link_config */
    foreach ($entities as $id => $menu_link_config) {
      $this->derivatives[$id] = $menu_link_config->getPluginDefinition() + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}

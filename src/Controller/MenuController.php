<?php

namespace Drupal\menu_link_config\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\system\MenuInterface;

/**
 * Class MenuController
 *
 * @package Drupal\menu_link_config\Controller
 */
class MenuController extends ControllerBase {

  /**
   * Provides the config menu link creation form.
   *
   * @param \Drupal\system\MenuInterface $menu
   *   An entity representing a custom menu.
   *
   * @return array
   *   Returns the menu link creation form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function addLink(MenuInterface $menu) {
    $menu_link = $this
      ->entityTypeManager()
      ->getStorage('menu_link_config')
      ->create([
        'id' => '',
        'parent' => '',
        'menu_name' => $menu->id(),
        'bundle' => 'menu_link_config',
      ]);

    return $this->entityFormBuilder()->getForm($menu_link);
  }

}

<?php

namespace Drupal\Tests\menu_link_config\Unit;

use Drupal\menu_link_config\Plugin\Menu\MenuLinkConfig;
use Drupal\Tests\UnitTestCase;

/**
 * Defines unit tests.
 *
 * @group Menu
 */
class MenuLinkConfigTest extends UnitTestCase {

  /**
   * Test plugin derivative id.
   */
  public function testDerivativeId() {
    /** @var \Drupal\menu_link_config\Plugin\Menu\MenuLinkConfig $menu_link_content_plugin */
    $menu_link_content_plugin = $this->prophesize(MenuLinkConfig::class);
    $menu_link_content_plugin->getDerivativeId()->willReturn('test_id');
    $menu_link_content_plugin = $menu_link_content_plugin->reveal();

    $this->assertEquals('test_id', $menu_link_content_plugin->getDerivativeId());
  }
  
}
